#!/bin/bash
cd Vagrantfile
vagrant up
cd ..
#create venv if not exist
python3 -m venv HotLocsEnv
###
#if linux
./HotLocsEnv/bin/activate
#if  windows
.\HotLocsEnv\Scripts\activate


#install requirements
cd .\backend\
pip install -r .\requirements.txt

cd .\django


python.exe .\manage.py runserver 7563

#lanciamo frontend su altro backend
cd .\frontend\hotlocs
npm install 
npm start

##popola esempio
python.exe .\main.py --multi --city Genova --reload 