# INDEX
1. [Intro](#intro)  
2. [System start-up](#ssu)   

<div id='intro' \div>

## Intro

Here the general definition of the Project.

<div id='ssu' \div>

## System start-up

In order to start the hotlocs system follow the futher steps: 
(every terminal command suppose to be in ```.../hotlocs_docker/``` folder)

* start Docker application;
* start MongoDB Compass application;
* start vagrant : 

	```	
	$cd Vagrantfile
	$vagrant up 
	```
* start the docker container:

	```	
	$docker start execution_server
	$docker ps -a #to check the docker status
	```
* start the Backend: 

	First, start the virtualEnv
	
	```	
	$source HotLocsEnv/bin/activate
	```
	Then, start the django server
	
	```	
	$ cd backend/django
	$python3 manage.py runserver 7563
	```
* start the Frontend:

	```	
	$cd frontend/hotlocs/
	$npm start
	```
	
Well done! Now the system is up. The next step can be to add a new city to the system. Thus, go to the homepage of the local website, and follow the instruction:

* make sure you have the gtfs files in ```.../hotlocs_docker/backend/django/hotlocs/Data/input/ ```;
* in homepage website, go to create city (eg Milan) and add the following values: 
	
	```	
	Milan
	N 45.525
	S 45.425
	W 9.09
	E 9.28
	```

Check in the MongoDB Compass aplication, after a refresh, if the city is correctly loaded. 
