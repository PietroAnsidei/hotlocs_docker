import React from 'react';
import Layout from './components/Layout';
import Urls from './Urls';


function App() {
  return (
    <div className="App">
      <Layout>
         <Urls/>
      </Layout>
    </div>
  );
}
export default App;