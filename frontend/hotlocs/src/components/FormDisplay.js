import React from "react";
import { Button } from '@material-ui/core';

const FormDisplay = props => {

  const { destinationList, delete_func } = props;

  const dest_list = destinationList.map(prop_object => {
    return (
      <div className="Display" key={prop_object.name}>

        <div>Label: {prop_object.name}</div>
        <div>Address: {prop_object.address}</div>
        <div>Latitude: {prop_object.lat}</div>
        <div>Longitude: {prop_object.lon}</div>

        <Button variant="contained"
          onClick={() => {
            delete_func(prop_object.name);
          }} // Surround it with an anonymous function so that the function call is not made
          > Delete destination </Button>
      </div>
    );
  });

  return <div className="displayform">{dest_list}</div>;
};

export default FormDisplay;