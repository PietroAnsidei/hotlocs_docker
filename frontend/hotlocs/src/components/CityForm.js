import React, { Component } from "react";
import { Button } from '@material-ui/core';

class CityForm extends Component {

  state = {
        city: null,
        north: null,
        south: null,
        west:  null,
        east:  null,
  };

  destinationFormFieldChange = e => {
    this.setState({
      [e.target.id]: e.target.value
    });
  };

  destinationSubmit = e => {
    e.preventDefault();
    this.props.addAppState(this.state);
  };

  render() {
    return (
      <div>
        <form onSubmit={this.destinationSubmit}>
          <p> <label htmlFor="city">City:</label>
          <input type="text" id="city" onChange={this.destinationFormFieldChange} /> </p>
          <p> <label htmlFor="north">North:</label>
          <input type="text" id="north" onChange={this.destinationFormFieldChange} /> </p>
          <p> <label htmlFor="south">South:</label>
          <input type="text" id="south" onChange={this.destinationFormFieldChange}/> </p>
          <p> <label htmlFor="west">West:</label>
          <input type="text" id="west" onChange={this.destinationFormFieldChange}/> </p>
          <p> <label htmlFor="east">East:</label>
          <input type="text" id="east" onChange={this.destinationFormFieldChange}/> </p>
          <Button variant="contained" onClick={this.destinationSubmit}>Add Values</Button>
        </form>
      </div>
    );
  }
}

export default CityForm;