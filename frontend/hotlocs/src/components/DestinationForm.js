import React, { Component } from "react";
import { Button } from '@material-ui/core';

class DestinationForm extends Component {

  state = {
    name: "None",
    address: "None",
    lat: "None",
    lon: "None"
  };

  destinationFormFieldChange = e => {
    this.setState({
      [e.target.id]: e.target.value
    });
  };

  destinationSubmit = e => {
    e.preventDefault();
    this.props.addAppState(this.state);
  };

  render() {
    return (
      <div>
        <form onSubmit={this.destinationSubmit}>
          <p> <label htmlFor="name">Label:</label>
          <input type="text" id="name" onChange={this.destinationFormFieldChange} /> </p>
          <p> <label htmlFor="address">Address:</label>
          <input type="text" id="address" onChange={this.destinationFormFieldChange} /> </p>
          <p> <label htmlFor="lat">Latitude:</label>
          <input type="text" id="lat" onChange={this.destinationFormFieldChange}/> </p>
          <p> <label htmlFor="lon">Longitude:</label>
          <input type="text" id="lon" onChange={this.destinationFormFieldChange}/> </p>
          <Button variant="contained" onClick={this.destinationSubmit}>Add destination</Button>
        </form>
      </div>
    );
  }
}

export default DestinationForm;