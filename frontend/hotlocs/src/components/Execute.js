import React from "react";
import axios from 'axios';
import CssBaseline from '@material-ui/core/CssBaseline';
import { Container, Grid, Paper, Typography, Button } from '@material-ui/core';

import * as settings from '../settings';
import FormManager from "./FormManager";
import CityMenu from "./CityMenu";
import GenMenu from "./GenMenu";

class Execute extends React.Component{
    constructor(props) {
        super(props);

        this.state = {
            city: null,
            gen_ts_creation: 'latest',
            destinations: [],
            execution: null,
            sub_query_done: false
        };
    };

    setCity = (city) => {
        this.setState({
            city: city,
            gen_ts_creation: this.state.gen_ts_creation,
            destinations: this.state.destinations,
            execution: this.state.execution,
            sub_query_done: false
        });
    };

    setGeneration = (gen_ts_creation) => {
        this.setState({
            city: this.state.city,
            gen_ts_creation: gen_ts_creation,
            destinations: this.state.destinations,
            execution: this.state.execution,
            sub_query_done: true
        });
    };

    setDestinations = form_state => {
        console.log(form_state)
        this.setState({
            city: this.state.city,
            gen_ts_creation: this.state.gen_ts_creation,
            destinations: form_state.destinations,
            execution: null,
            sub_query_done: this.state.sub_query_done
        });
    };

    setExecution = (res) => {
        this.setState({
            city: this.state.city,
            gen_ts_creation: this.state.gen_ts_creation,
            destinations: this.state.destinations,
            execution: res.data,
            sub_query_done: this.state.sub_query_done
        });
        this.props.setExecution(res);
    };


    // Function to make the predict API call and update the state variable - Prediction
    handleExecute = (event) => {

        let dataform = {
            city: this.state.city,
            gen_ts_creation: this.state.gen_ts_creation,
            destinations: this.state.destinations
        };

        console.log(this.state);
        // Axios variables required to call the predict API
        let headers = {} // { 'Authorization': `Token ${props.token}` };
        let url = settings.API_SERVER + '/api/execute/';
        let method = 'post';
        let config = { headers, method, url, data: dataform };

        axios(config).then(
            res => {this.setExecution(res)
            }).catch(
                error => {alert(error)});
    };

    render() {
        return (
            <React.Fragment>
                <CssBaseline />

                <Typography variant="h3">
                    Fetch city map
                </Typography>

                <Container fixed className={this.props.container}>
                    <Grid container alignItems="center" spacing={3}>
                        <Grid item xs={6}>
                            <Paper className={this.props.title} elevation={0}>
                                <Typography variant="h5">
                                    City: {this.state.city}
                                </Typography>
                                <CityMenu className={this.props.formControl} setCity={this.setCity}/>
                            </Paper>
                            <Paper className={this.props.title} elevation={0}>
                                <Typography variant="h5">
                                    Generation: {this.state.gen_ts_creation}
                                </Typography>
                                <GenMenu
                                    className={this.props.formControl}
                                    city={this.state.city}
                                    query_done={this.state.sub_query_done}
                                    setGeneration={this.setGeneration} />
                            </Paper>

                            <Paper className={this.props.title}>
                                <FormManager setDestinations={this.setDestinations}/>
                            </Paper>

                        </Grid>
                        <Grid item xs={2}>
                            <Button variant="contained" color="primary" onClick={this.handleExecute}>
                                Execute!
                            </Button>
                        </Grid>
                        <Grid item xs={4}>
                            <Paper className={this.props.title} elevation={0}>
                                <Typography variant="caption" display="inline">
                                    Result: <span>&nbsp;</span>
                                </Typography>
                                <Typography variant="body1" display="inline">
                                    {JSON.stringify(this.state.execution)}
                                </Typography>
                            </Paper>
                        </Grid>
                    </Grid>
                </Container>
            </React.Fragment>
        );
    }
}

export default Execute;