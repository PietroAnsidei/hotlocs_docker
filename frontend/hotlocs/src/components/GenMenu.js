import React from 'react';
import axios from 'axios';

import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';

import * as settings from '../settings';

class GenMenu extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            openMenu: false,
            queryDone: this.props.query_done,
            data: {
                menu: []
            },
            city: this.props.city,
            generation: 'latest',
            anchorEl: null
        };
    };

    componentWillReceiveProps(props) {
        this.setState({
            openMenu: false,
            queryDone: this.props.query_done,
            data: {
                menu: []
            },
            city: this.props.city,
            generation: 'latest',
            anchorEl: null
        });
    }

    setData = (dataform) => {
        this.setState({
            openMenu: this.state.openMenu,
            queryDone: true,
            data: dataform,
            generation: this.state.generation,
            anchorEl: this.state.anchorEl
        });
        console.log("Data set. State?");
        console.log(this.state);
    };

    handlePostQuery = () => {

        let dataform = {
            fk_city_name: this.state.city
        };
        console.log("Posting. State?");
        console.log(this.state);

        if (!this.state.queryDone) {

            let headers = {}; // { 'Authorization': `Token ${props.token}` };
            let method = 'post';
            let url = settings.API_SERVER + '/api/generations_get/';
            let config = { headers, method, url, data: dataform };

            axios(config).then(
                res => {this.setData(res.data)
                }).catch(
                    error => {alert(error)});
        };
    };

    setOpenMenu = (event) => {
        console.log("Menu Interaction!")
        this.setState({
            openMenu: !this.state.openMenu,
            queryDone: this.state.queryDone,
            data: this.state.data,
            generation: this.state.generation,
            anchorEl: event
        });
    };

    handleClick = (event) => {
        this.handlePostQuery();
        this.setOpenMenu(event.currentTarget);
    };

    setGeneration = (event) => {
        console.log("Generation set!")
        this.props.setGeneration(event.target.getAttribute("primarytext"));
    };

    handleClose = (event) => {
        this.setOpenMenu(null);
        this.setGeneration(event);
    };

    renderMenuItems = () => {
        return (
            <div> { this.state.data.menu.length > 0 ? this.state.data.menu.map((item, index) => {
                    return (
                        <MenuItem key={item.idx} primarytext={item.ts_creation} onClick={this.handleClose}>
                            {item.ts_creation} </MenuItem>
                    );
                }) : null }
            </div>
        );
    };

    render(){
        return (
            <div>
                <Button aria-controls="simple-menu" aria-haspopup="true" color="primary"
                    onClick={this.handleClick}>
                    Select generation...
                </Button>
                <Menu
                    id="simple-menu"
                    anchorEl={this.state.anchorEl}
                    keepMounted
                    open={this.state.openMenu}
                    onClose={this.handleClose}
                >
                    { this.renderMenuItems() }
                </Menu>
            </div>
        );
    };
}

export default GenMenu;