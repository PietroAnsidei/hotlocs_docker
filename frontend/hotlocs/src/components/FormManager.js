import React, { Component } from "react";
import FormDisplay from "./FormDisplay";
import DestinationForm from "./DestinationForm";

import { Button } from '@material-ui/core';

class FormManager extends Component {

  state = {destinations: []};

  setDestinations = () => {
    this.props.setDestinations(this.state);
  };

  addAppState = app_state => {
    let state_list = [...this.state.destinations, app_state];
    this.setState({destinations: state_list});
  };

  deleteAppState = id => {
    let new_state_list = this.state.destinations.filter(state_object => {
      return state_object.name !== id;
    });
    this.setState({destinations: new_state_list});
  };

  render() {
    return (
      <div className="form_manager">
        <FormDisplay
          destinationList={this.state.destinations}
          delete_func={this.deleteAppState}
        />
        <DestinationForm addAppState={this.addAppState}/>
        <Button variant="contained" color="primary" onClick={this.setDestinations}> Submit destinations </Button>
      </div>
    );
  }
}

export default FormManager;