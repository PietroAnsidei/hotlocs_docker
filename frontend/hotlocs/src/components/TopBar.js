import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { AppBar, Toolbar, Typography, Button, IconButton } from '@material-ui/core';
import HomeIcon from '@material-ui/icons/Home';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
}));

export default function ButtonAppBar() {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar>
          <Typography variant="h6" className={classes.title}>
            HotLocs
          </Typography>
          <IconButton aria-label="home page" color="inherit" href="/">
            <HomeIcon />
          </IconButton>
          <Button color="inherit" href="/create_city">Create City</Button>
          <Button color="inherit" href="/generate">Generate</Button>
          <Button color="inherit" href="/execute">Execute</Button>
          {/* <Button color="inherit" href="/showmap">Show Map</Button> */}
        </Toolbar>
      </AppBar>
    </div>
  );
}
