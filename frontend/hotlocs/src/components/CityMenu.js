import React from 'react';
import axios from 'axios';

import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';

import * as settings from '../settings';

class CityMenu extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            openMenu: false,
            queryDone: false,
            data: {
                menu: []
            },
            city: null,
            anchorEl: null
        };
    };

    setData = (dataform) => {
        this.setState({
            openMenu: this.state.openMenu,
            queryDone: true,
            data: dataform,
            city: this.state.city,
            anchorEl: this.state.anchorEl
        });
    };

    handleGetQuery = () => {

        if (!this.state.queryDone) {

            let headers = {}; // { 'Authorization': `Token ${props.token}` };
            let method = 'get';
            let url = settings.API_SERVER + '/api/cities_get/';
            let config = { headers, method, url };

            axios(config).then(
                res => {this.setData(res.data)
                }).catch(
                    error => {alert(error)});

        };
    };

    setOpenMenu = (event) => {
        console.log("Menu Interaction!")
        this.setState({
            openMenu: !this.state.openMenu,
            queryDone: this.state.queryDone,
            data: this.state.data,
            city: this.state.city,
            anchorEl: event
        });
    };

    handleClick = (event) => {
        this.setOpenMenu(event.currentTarget);
    };

    setCity = (event) => {
        console.log("City set!")
        this.props.setCity(event.target.getAttribute("primarytext"));
    };

    handleClose = (event) => {
        this.setOpenMenu(null);
        this.setCity(event);
    };

    renderMenuItems = () => {
        return (
            <div> { this.state.data.menu.length > 0 ? this.state.data.menu.map((item, index) => {
                    return (
                        <MenuItem key={item.idx} primarytext={item.city} onClick={this.handleClose}> {item.city}
                            </MenuItem>
                    );
                }) : null }
            </div>
        );
    };

    render(){
        this.handleGetQuery();
        return (
            <div>
                <Button aria-controls="simple-menu" aria-haspopup="true" color="primary"
                    onClick={this.handleClick}>
                    Select city...
                </Button>
                <Menu
                    id="simple-menu"
                    anchorEl={this.state.anchorEl}
                    keepMounted
                    open={this.state.openMenu}
                    onClose={this.handleClose}
                >
                    { this.renderMenuItems() }
                </Menu>
            </div>
        );
    };
}

export default CityMenu;