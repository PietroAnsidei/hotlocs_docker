import React from 'react'
import axios from 'axios';
import * as settings from '../settings';

import CityMenu from "./CityMenu";

import CssBaseline from '@material-ui/core/CssBaseline';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import { Container, Grid, Paper, Typography, Slider, Button } from '@material-ui/core';

// ########################################################
// Material UI inline styles
// ########################################################
const useStyles = makeStyles((theme) => ({
    container: {
        maxWidth: "75%",
        marginTop: "15vh",
        marginBottom: "10vh",
        borderRadius: '6px',
        backgroundColor: theme.palette.action.disabledBackground,
    },
    title: {
        marginTop: theme.spacing(2),
        marginBottom: theme.spacing(2),
        padding: theme.spacing(2), paddingLeft: theme.spacing(4),
        color: theme.palette.primary.main,
    },
    sliders: {
        paddingTop: theme.spacing(2),
        paddingBottom: theme.spacing(2),
        paddingLeft: theme.spacing(4),
        paddingRight: theme.spacing(4),
        marginBottom: theme.spacing(2),
    },
    slidertop: {
        marginTop: theme.spacing(2),
    },
    formControl: {
        margin: theme.spacing(4),
        minWidth: 120,
        color: theme.palette.primary.main,
    }
}));

// ########################################################
// Our Custom IRIS slider. You may use the default slider instead of this
// ########################################################
const HotLocsSlider = withStyles({
    root: {
        color: '#751E66',
    },
    valueLabel: {
        left: 'calc(-50% -2)',
        top: -22,
        '& *': {
            background: 'transparent',
            color: '#000',
        },
    },
    mark: {
        height: 8,
        width: 1,
        marginTop: -3,
    },
    markActive: {
        opacity: 1,
        backgroundColor: 'currentColor',
    },
})(Slider);

// Marks on the slider track
const marks_grid = [{ value: 1, label: "1" }, { value: 100, label: "100" }];
const marks_walk = [{ value: 0, label: "False" }, { value: 1, label: "True" }];

// ########################################################
// The main Home component returned by this Module
// ########################################################
function Generate(props) {
    // Material UI Classes
    const classes = useStyles();

    // React hook state variable - Dimensions
    const [dimensions, setDimensions] = React.useState({
        grid_size_lat: 5,
        grid_size_lon: 3,
        walking: 0
    });
    // React hook state variable - Prediction
    const [generation, setGeneration] = React.useState(null);

    const [city, setCity] = React.useState(null);

    // Function to update the Dimensions state upon slider value change
    const handleSliderChange = name => (event, newValue) => {
        setDimensions(
            {
                ...dimensions,
                ...{ [name]: newValue }
            }
        );
    };

    const bool2Str = (s) => {
      return s.toString().charAt(0).toUpperCase() + s.toString().slice(1)
    }

    // Function to make the predict API call and update the state variable - Prediction
    const handleGenerate = event => {
        let dataform = {
            "fk_city_name": city,
            "grid_size_lat": dimensions.grid_size_lat,
            "grid_size_lon": dimensions.grid_size_lon,
            "walking": bool2Str(!!dimensions.walking)
        }

        //Axios variables required to call the predict API
        let headers = {} // 'Authorization': `Token ${props.token}` };
        let url = settings.API_SERVER + '/api/generations_put/';
        let method = 'post';
        let config = { headers, method, url, data: dataform };

        //Axios predict API call
        axios(config).then(
            res => {setGeneration(res.data)
            }).catch(
                error => {alert(error)})
    }

    return (
        <React.Fragment>
            <CssBaseline />

            <Typography variant="h3">
                Generate city time matrix
            </Typography>

            <Container fixed className={classes.container}>
                <Grid container alignItems="center" spacing={3}>
                    <Grid item xs={6}>
                        <Paper className={classes.title} elevation={0}>
                            <Typography variant="h5">
                                City: {city}
                            </Typography>
                            <CityMenu className={classes.formControl} setCity={setCity}/>
                        </Paper>
                        <Paper className={classes.sliders}>
                            <Typography id="grid_size_lat" variant="caption" >
                                Grid size (latitude)
                            </Typography>
                            <HotLocsSlider
                                defaultValue={5}
                                aria-labelledby="grid_size_lat"
                                step={1}
                                min={1}
                                max={100}
                                valueLabelDisplay="on"
                                marks={marks_grid}
                                className={classes.slidertop}
                                onChange={handleSliderChange("grid_size_lat")}
                            />
                            <Typography id="grid_size_lon" variant="caption" >
                                Grid size (longitude)
                            </Typography>
                            <HotLocsSlider
                                defaultValue={3}
                                aria-labelledby="grid_size_lon"
                                step={1}
                                min={1}
                                max={100}
                                valueLabelDisplay="on"
                                marks={marks_grid}
                                className={classes.slidertop}
                                onChange={handleSliderChange("grid_size_lon")}
                            />
                            <Typography id="walking" variant="caption" >
                                Walking mode
                            </Typography>
                            <HotLocsSlider
                                disabled="true"
                                defaultValue={0}
                                aria-labelledby="walking"
                                step={1}
                                min={0}
                                max={1}
                                valueLabelDisplay="off"
                                marks={marks_walk}
                                className={classes.slidertop}
                                onChange={handleSliderChange("walking")}
                            />
                        </Paper>
                    </Grid>
                    <Grid item xs={2}>
                        <Button variant="contained" color="primary" onClick={handleGenerate}>
                            Generate!
                        </Button>
                    </Grid>
                    <Grid item xs={4}>
                        <Paper className={classes.title} elevation={0}>
                            <Typography variant="caption" display="inline">
                                Result: <span>&nbsp;</span>
                            </Typography>
                            <Typography variant="body1" display="inline">
                                {JSON.stringify(generation)}
                            </Typography>
                        </Paper>
                    </Grid>
                </Grid>
            </Container>
        </React.Fragment>
    )
}

export default Generate