import React from 'react'
import CssBaseline from '@material-ui/core/CssBaseline';
import Iframe from 'react-iframe'

function Showmap(props) {

    var execution = ""
    if (props.location.state && props.location.state.execution) {execution = props.location.state.execution}
    var path = "http://localhost/".concat(execution);

    return (
        <React.Fragment>
            <CssBaseline />
                {/* {fullpath.length>0 ? <h3>{fullpath} </h3> : <h1>Ooops! No map to show! </h1>} */}
                <Iframe url={path}
                    width="100%" height="768px" id="map_el" className="map" display="center" position="center"></Iframe>
        </React.Fragment>
    )
}
export default Showmap