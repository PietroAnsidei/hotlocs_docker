import React from "react";
import { Redirect } from "react-router-dom";
import { makeStyles } from '@material-ui/core/styles';

import Execute from "./Execute";

// ########################################################
// Material UI inline styles
// ########################################################

const useStyles = makeStyles((theme) => ({
    container: {
        maxWidth: "75%",
        marginTop: "15vh",
        marginBottom: "10vh",
        borderRadius: '6px',
        backgroundColor: theme.palette.action.disabledBackground,
    },
    title: {
        marginTop: theme.spacing(2),
        marginBottom: theme.spacing(2),
        marginRight: theme.spacing(2),
        marginLeft: theme.spacing(2),
        padding: theme.spacing(4),
        paddingLeft: theme.spacing(4),
        color: theme.palette.primary.main,
    },
    formControl: {
        margin: theme.spacing(4),
        minWidth: 120,
        color: theme.palette.primary.main,
    }
}));

class ExecutePageClass extends React.Component{

    constructor(props) {
        super(props);

        this.state = {
            execution: {
                html_path: null
            }
        };
    };

    setExecution = (res) => {this.setState({execution: res.data});};

    render() {
        return (
            <React.Fragment>
                <Execute {...this.props} setExecution={this.setExecution}/>
                {this.state.execution.html_path ? <Redirect to={{
                    pathname: "/showmap",
                    state: { execution: this.state.execution.html_path }
                }} /> : null}
            </React.Fragment>
        );
    };
};

function ExecutePage(props){
    const classes = useStyles();
    return (
        <ExecutePageClass {...classes}/>
    );
};

export default ExecutePage