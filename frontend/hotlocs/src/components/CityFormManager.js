import React, { Component } from "react";
import CityForm from "./CityForm";
import CityFormDisplay from "./CityFormDisplay";

import { Button } from '@material-ui/core';

class CityFormManager extends Component {

  state = {
    city: null,
    north: null,
    south: null,
    west: null,
    east: null,
  };

  setDimensions = () => {
    this.props.setDimensions(this.state);
    console.log(this.state)
  };

  addAppState = app_state => {
    this.setState(app_state);
  };

  deleteAppState = (default_state) => {
    this.setState({
        city: null,
        north: null,
        south: null,
        west: null,
        east: null,
    });
  };

  render() {
    return (
      <div className="form_manager">
        <CityFormDisplay state={this.state} delete_func={this.deleteAppState}/>
        <Button variant="contained" color="primary" onClick={this.setDimensions}> Submit values </Button>
        <CityForm addAppState={this.addAppState}/>
      </div>
    );
  }
}

export default CityFormManager;