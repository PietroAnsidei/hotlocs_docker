import React from "react";
import { Button } from '@material-ui/core';

const CityFormDisplay = props => {

  const { state, delete_func } = props;

  return  <div className="Display">
    <div>City: {state.city}</div>
    <div>North: {state.north}</div>
    <div>South: {state.south}</div>
    <div>East: {state.east}</div>
    <div>West: {state.west}</div>
    <Button variant="contained" onClick={delete_func} > Delete setting </Button>
    {/* Surround it with an anonymous function so that the function call is not made */}
  </div>
};

export default CityFormDisplay;