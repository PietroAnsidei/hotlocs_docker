import React, { Component } from 'react'
import DragAndDrop from './DragAndDrop'
import { Button } from '@material-ui/core';

class DADHandler extends Component {

  state = {files: []}

  handleDrop = (files) => {
    let fileList = this.state.files
    for (var i = 0; i < files.length; i++) {
      if (!files[i].name) return
      fileList.push(files[i].name)
    }
    this.setState({files: fileList})
  }

  handleRemove = () => {this.setState({files: []})}

  render() {
    {/* DO STH INSTEAD OF JUST SHOW */}
    return (
      <div>
        <div> List of uploaded files: </div>
        <DragAndDrop handleDrop={this.handleDrop}>
          <div style={{height: 300, width: 250}} alignItems="center" justifyContent="center">
            { this.state.files.map(
              (file, index) => <div key={index}>{file}</div> )
            }
          </div>
        </DragAndDrop>
        <Button variant="contained" color="primary" onClick={this.handleRemove}> Clear Files! </Button>
      </div>
    )
  }
}
export default DADHandler