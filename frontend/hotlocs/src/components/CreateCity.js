import React from 'react'
import axios from 'axios';
import * as settings from '../settings';

import CssBaseline from '@material-ui/core/CssBaseline';
import { makeStyles } from '@material-ui/core/styles';
import { Container, Grid, Paper, Typography, Button } from '@material-ui/core';

import CityFormManager from "./CityFormManager";
import DADHandler from "./DADHandler"

// ########################################################
// Material UI inline styles
// ########################################################
const useStyles = makeStyles((theme) => ({
    container: {
        maxWidth: "75%",
        marginTop: "15vh",
        marginBottom: "10vh",
        borderRadius: '6px',
        backgroundColor: theme.palette.action.disabledBackground,
    },
    title: {
        marginTop: theme.spacing(2),
        marginBottom: theme.spacing(2),
        padding: theme.spacing(2), paddingLeft: theme.spacing(4),
        color: theme.palette.primary.main,
    },
}));

// ########################################################
// The main Home component returned by this Module
// ########################################################
function CreateCity(props) {
    // Material UI Classes
    const classes = useStyles();

    // React hook state variable - Dimensions
    const [dimensions, setDimensions] = React.useState({
        city: null,
        north: null,
        south: null,
        west:  null,
        east:  null,
    });

    // React hook state variable - Prediction
    const [done, setDone] = React.useState({bool: false});

    // Function to make the predict API call and update the state variable - Prediction
    const handleGenerate = event => {
        let dataform = {
            "city_name": dimensions.city,
            "map_b": {
                "north": dimensions.north,
                "south": dimensions.south,
                "west": dimensions.west,
                "east": dimensions.east
            }
        }

        //Axios variables required to call the predict API
        let headers = {} // 'Authorization': `Token ${props.token}` };
        let url = settings.API_SERVER + '/api/cities_put/';
        let method = 'post';
        let config = { headers, method, url, data: dataform };

        //Axios predict API call
        axios(config).then(
            () => {setDone({bool: true})
            }).catch(
                error => {alert(error)})

    }

    return (
        <React.Fragment>
            <CssBaseline />

            <Typography variant="h3">
                Generate city
            </Typography>

            <Container fixed className={classes.container}>
                <Grid container alignItems="center" spacing={3}>
                    <Grid item xs={6}>
                        <Paper className={classes.title}>
                            <CityFormManager setDimensions={setDimensions}/>
                        </Paper>
                        <Paper> <DADHandler/> </Paper>
                    </Grid>
                    <Grid item xs={2}>
                        <Button variant="contained" color="primary" onClick={handleGenerate}>
                            Submit!
                        </Button>
                    </Grid>
                    <Grid item xs={4}>
                        <Paper className={classes.title} elevation={0}>
                            <Typography variant="h5" display="inline">
                                Upload done: <span>&nbsp;</span> {done.bool.toString()}
                            </Typography>
                        </Paper>
                    </Grid>
                </Grid>
            </Container>
        </React.Fragment>
    )
}

export default CreateCity