import React from 'react'
import CssBaseline from '@material-ui/core/CssBaseline';
function Home() {
    return (
        <React.Fragment>
            <CssBaseline />
            <div>
                <h1>Welcome! </h1>
            </div>
        </React.Fragment>
    )
}
export default Home