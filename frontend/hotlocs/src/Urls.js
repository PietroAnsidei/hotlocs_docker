import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
// import Login from "./components/Login";
import Home from "./components/Home";
import CreateCity from "./components/CreateCity";
import Generate from "./components/Generate";
import ExecutePage from "./components/ExecutePage";
import Showmap from "./components/Showmap";

function Urls(props) {
    return (
        <div>
            <BrowserRouter>
                <Switch>
                    {/* <Route exact path="/login/"> <Login {...props} /></Route> */}
                    <Route exact path="/"> <Home {...props}/></Route>
                    <Route exact path="/create_city"> <CreateCity {...props}/></Route>
                    <Route exact path="/generate"> <Generate {...props}/></Route>
                    <Route exact path="/execute"> <ExecutePage {...props}/></Route>
                    <Route exact path="/showmap" render={(props) => <Showmap {...props}/>}/>
                </Switch>
            </BrowserRouter>
        </div>
    )
};
export default Urls;