from django.urls import path
import hotlocs.views as views

urlpatterns = [
    path('cities_get/', views.CitiesAPI.as_view(), name='cities_get'),
    path('cities_put/', views.CitiesAPI.as_view(), name='cities_put'),
    path('generations_get/', views.GenerationsGetApi.as_view(), name='generations_get'),
    path('generations_put/', views.GenerationsSetApi.as_view(), name='generations_put'),
    path('execute/', views.Execute.as_view(), name='execute'),
]
