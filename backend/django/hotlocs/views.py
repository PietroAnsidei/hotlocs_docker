import googlemaps
# from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from .apps import *
from hotlocs.hotlocs.Utils.Setup import *
from hotlocs.hotlocs.Utils.City import *
from hotlocs.hotlocs.Utils.Generation import *
from hotlocs.hotlocs.Utils.Execution import *


class CitiesAPI(APIView):

    # permission_classes = [IsAuthenticated]

    def get(self, request, format=None):

        # Setup main params
        config = HotlocsQueryConfig.u_args
        parameters = global_params(config)

        # Setup db connection & google api key
        database = db_config(parameters)

        # Process data and output
        response = query_cities(parameters, database)

        return Response(response, status=200)

    def post(self, request, format=None):

        # Setup main params
        config = HotlocsQueryConfig.u_args
        parameters = global_params(config)

        # Setup db connection & google api key
        database = db_config(parameters)

        # Process data and output
        response = post_cities(parameters, database, request)

        return Response(response, status=200)


class GenerationsGetApi(APIView):

    # permission_classes = [IsAuthenticated]

    def post(self, request, format=None):

        # Setup main params
        config = HotlocsQueryConfig
        u_args = set_gen_prop(config, request.data)
        parameters = global_params(u_args)

        # Setup db connection & google api key
        database = db_config(parameters)

        # Process data and output
        response = query_generations(parameters, database)

        return Response(response, status=200)


class GenerationsSetApi(APIView):

    # permission_classes = [IsAuthenticated]

    def post(self, request):

        # Setup main params
        config = HotlocsGenConfig
        u_args = set_gen_prop(config, request.data)
        parameters = global_params(u_args)

        # Setup db connection & google api key
        database = db_config(parameters)

        # Create objects
        execution = Execution(parameters, u_args)
        city = City.load_or_create(parameters, database, execution)
        generation = Generation.load_or_create(parameters, database, city, execution, request.data)

        # Process data and output
        generation.process(parameters, database, city)
        response = generation.get_response()

        return Response(response, status=200)


class Execute(APIView):

    # permission_classes = [IsAuthenticated]

    def post(self, request):

        # Setup main params
        u_args = HotlocsExecConfig.u_args
        if 'city' in request.data:
            u_args['city'] = request.data['city']
        if 'gen_ts_creation' in request.data:
            u_args['generation'] = request.data['gen_ts_creation']

        parameters = global_params(u_args)

        # Setup db connection & google api key
        database = db_config(parameters)
        gmaps = googlemaps.Client(key=parameters['g_key'])

        # Create objects
        execution = Execution(parameters, u_args, request.data)
        city = City.load_or_create(parameters, database, execution)
        generation = Generation.load_or_create(parameters, database, city, execution)

        # Process data and output
        generation.process(parameters, database, city)
        execution.process(parameters, database, gmaps, generation)
        response = {'html_path': execution.html_path.split("/")[-1]}

        return Response(response, status=200)

