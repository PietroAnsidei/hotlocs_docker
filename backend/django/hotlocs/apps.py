import os
from django.apps import AppConfig
from django.conf import settings


class HotlocsConfig(AppConfig):
    # BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    MODEL_FOLDER = os.path.join(settings.BASE_DIR, 'hotlocs/')
    os.chdir(MODEL_FOLDER)


class HotlocsExecConfig(HotlocsConfig):
    u_args = {
        'multi_thread': True,
        'reload': False,
        'city': 'Genova',
        'generation': 'latest'
    }


class HotlocsGenConfig(HotlocsConfig):
    u_args = {
        'multi_thread': True,
        'reload': True,
        'generation': None
    }


class HotlocsQueryConfig(HotlocsConfig):
    u_args = {
        'multi_thread': False,
        'reload': False,
        'generation': None
    }


def set_gen_prop(configs, request):
    configs.u_args['city'] = request['fk_city_name']
    return configs.u_args
