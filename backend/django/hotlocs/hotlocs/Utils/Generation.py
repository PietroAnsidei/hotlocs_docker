import json
import logging
import multiprocessing as mp
import numpy as np
import pandas as pd

from datetime import datetime, timedelta, date, time

logging.basicConfig(level=logging.INFO, format='%(asctime)s [%(levelname)s] %(message)s', datefmt='%H:%M:%S')


class Generation:

    def __init__(self, _id=None, fk_city_name=None, fk_city_version=None, ts_creation=None, map_b=None, osm_file=None,
                 grid_size_lat=None, grid_size_lon=None, walking=False):

        if map_b is None:
            map_b = [
                {
                    'north': None,
                    'south': None,
                    'east': None,
                    'west': None
                }
            ]
        else:
            for key, value in map_b.items():
                map_b[key] = float(value)

        self._id = _id
        self.fk_city_name = fk_city_name
        self.fk_city_version = fk_city_version
        self.ts_creation = ts_creation
        self.map_b = map_b
        self.osm_file = osm_file
        self.grid_size_lat = grid_size_lat
        self.grid_size_lon = grid_size_lon
        self.size = self.grid_size_lon * self.grid_size_lat
        self.walking = walking
        self.walking_speed_kph = 5
        self.lat_walk_time_for_cent = int(np.round(111.1 / 100 / self.walking_speed_kph * 60))
        self.lon_walk_time_for_cent = int(
            np.round(self.lat_walk_time_for_cent * np.cos(np.deg2rad(self.map_b['north']))))

        grid_lon, grid_lat = np.mgrid[
                             self.map_b['west']:self.map_b['east']:complex(self.grid_size_lon, 1),
                             self.map_b['north']:self.map_b['south']:complex(self.grid_size_lat, 1)]

        self.positions = np.vstack([grid_lat.ravel(), grid_lon.ravel()]).T
        self.timetable = None
        self.grid = None

    @classmethod
    def from_dict(cls, json_dict):
        return cls(**json_dict)

    def to_json(self):
        return dict(fk_city_name=self.fk_city_name, fk_city_version=self.fk_city_version, ts_creation=self.ts_creation,
                    osm_file=self.osm_file, grid_size_lat=self.grid_size_lat, grid_size_lon=self.grid_size_lon,
                    walking=self.walking)

    def get_id(self):
        return str(self._id)

    def get_response(self):
        response = {
            "fk_city_name": self.fk_city_name,
            "ts_creation": self.ts_creation}
        return response

    @staticmethod
    def load_or_create(parameters, database, city, execution, request=None):
        col = database[parameters['coll_generations']]
        flag_export = False

        if parameters['reload']:
            flag_export = True
            logging.info('Creating generation from input')
            if not request:
                logging.info('Creating generation from settings file')
                with open('{}generation.json'.format(parameters['input_folder']), 'r') as file:
                    request = json.loads(file.read().replace('\n', ''))

        else:
            try:
                logging.info('Importing generation data from db')

                if execution.generation == 'latest':
                    request = col.find({"fk_city_name": city.city_name}).sort("ts_creation", -1).limit(1)[0]

                else:
                    request = col.find(
                        {"fk_city_name": city.city_name,
                         "ts_creation": execution.generation})[0]

            except Exception as e:
                logging.error(e)

        request['map_b'] = city.map_b
        generation = Generation.from_dict(request)
        generation.fk_city_version = city.version
        if not generation.ts_creation:
            generation.ts_creation = datetime.strftime(datetime.now(), parameters['datetime_fmt'])

        if flag_export:
            logging.info('Exporting data to db')
            generation._id = col.insert_one(generation.to_json()).inserted_id

        logging.info('Operation complete!')

        return generation

    def process(self, parameters, database, city):

        if parameters['reload']:
            # Create grid and parameters
            self.timetable_setup(parameters, city)
            self.time_grid_setup(parameters)
            self.dump_data(parameters, database)
        else:
            # Load grid and parameters
            self.load_data(parameters, database)

    def timetable_setup(self, parameters, city):
        """ Transform the public transportation stops into their closest approximation in the grid """

        logging.info('Timetable setup')

        stops = pd.DataFrame.from_records(city.stops).drop(['_id', 'city', 'version'], axis=1)
        times = pd.DataFrame.from_records(city.stop_times).drop(['_id', 'city', 'version'], axis=1)

        times['departure_time'] = pd.to_datetime(times['departure_time'], format=parameters['time_fmt']).dt.time
        times['arrival_time'] = pd.to_datetime(times['arrival_time'], format=parameters['time_fmt']).dt.time

        stops['anchor_id'] = stops.apply(lambda x:
                                         np.argmin(np.linalg.norm(self.positions -
                                                                  np.array((x.stop_lat, x.stop_lon)).reshape(1, -1),
                                                                  axis=1)), axis=1)
        stops['anchor_lat'] = stops.apply(lambda x: self.positions[x.anchor_id, 0], axis=1)
        stops['anchor_lon'] = stops.apply(lambda x: self.positions[x.anchor_id, 1], axis=1)

        self.timetable = pd.merge(times, stops, left_on='stop_id', right_on='stop_id', how='right')

    def time_grid_setup(self, parameters):
        """ Setup the distance matrix for every pair of points in the grid """

        logging.info('Computing time grid -- Multithread = {}'.format(parameters['multi_thread']))
        if parameters['multi_thread']:
            pool = mp.Pool(mp.cpu_count())
            results = pool.starmap(self.dijkstra_score,
                                   [(i_start, parameters) for i_start in range(self.size)])
            pool.close()

        else:
            results = [self.dijkstra_score(i_start, parameters) for i_start in range(self.size)]

        idx_list = [results[idx][0] for idx in range(self.size)]
        self.grid = [results[idx_list.index(idx)][1] for idx in range(self.size)]

    def dijkstra_score(self, idx_a, parameters):

        # Queue anchor points to be visited, then store their distance from origin and their parent in a nodes df
        queue = [i for i in range(self.size)]

        starting_time = datetime(100, 1, 1, 8, 0, 0).time()
        # starting_time = datetime.strptime(self.ts_creation, parameters['datetime_fmt']).time()

        # Initializations: index : anchor_id, distance : walking distance from origin, parent : origin
        if self.walking == 'True':
            walking_dist_from_origin = np.round(
                np.abs(self.positions - self.positions[idx_a, :]).dot(
                    np.array([100 * self.lat_walk_time_for_cent, 100 * self.lon_walk_time_for_cent]))
            ).tolist()
            walking_dist_from_origin = [int(x) for x in walking_dist_from_origin]
            max_dist_from_origin = int(max(walking_dist_from_origin))
            max_time = add_minutes(starting_time, max_dist_from_origin)

        else:
            walking_dist_from_origin = [np.inf] * len(queue)
            walking_dist_from_origin[idx_a] = 0
            max_time = datetime(100, 1, 1, 23, 59, 59).time()

        node_table = pd.DataFrame(
            list(zip(walking_dist_from_origin, [idx_a] * self.size)), index=queue,
            columns=['distance', 'parent'])

        timetable_red = self.timetable[self.timetable.departure_time.between(starting_time, max_time)]
        #logging.info("Anchor point no. {} - Original Timetable size: {}".format(idx_a, timetable_red.shape))

        # Up to when I reach every anchor
        while len(queue):

            if not len(queue) % 100:
                logging.info('Dijkstring anchor point {} / {} -- {:02d}% OVA grid -- {:02d}% Dijkstra'.format(
                    idx_a + 1, self.size, int(100 * idx_a / self.size),
                    int(100 * (1 - len(queue) / self.size))))


            # Find the closest anchor in the queue
            visited_anchor = node_table.iloc[node_table.index.isin(queue)].distance.idxmin()
            queue.remove(visited_anchor)

            # If the anchor is reachable from the origin
            if not node_table.loc[visited_anchor, 'distance'] == np.inf:

                # Filter the trips starting after the current time
                current_time = add_minutes(starting_time, node_table.loc[visited_anchor, 'distance'])
                timetable_red = timetable_red[timetable_red.departure_time.between(current_time, max_time)]
                #logging.info("Visiting anchor {} - Distance from origin = {}".format(visited_anchor, node_table.loc[
                #    visited_anchor, 'distance']))
                #logging.info("Current timestamp: {} - Timetable size: {}".format(current_time, timetable_red.shape))

                # For every available trip_id in the dataset
                for trip_id, line_trip in timetable_red.groupby(['trip_id']):

                    # If the trip visits the current anchor
                    if visited_anchor in line_trip.anchor_id.unique():

                        # Find when the trip steps by the anchor and filter only the subsequent stops in other anchors
                        anchor_stop_id = line_trip.stop_sequence[line_trip.anchor_id == visited_anchor].min()
                        next_stops = line_trip[
                            (line_trip.stop_sequence > anchor_stop_id) & ~(line_trip.anchor_id == visited_anchor)]

                        # For every available neighbor_id I can visit staying in the trip, which time do I reach it?
                        neighbor_toa = next_stops[['anchor_id', 'arrival_time']].groupby(['anchor_id']).agg('min')
                        neighbor_toa['neighbor_distance'] = neighbor_toa.arrival_time.apply(
                            lambda x: (x.hour - starting_time.hour) * 60 + x.minute - starting_time.minute)

                        for neighbor in neighbor_toa.index:
                            neighbor_distance = neighbor_toa.neighbor_distance.loc[neighbor]

                            # If it is sooner than I knew, update the path to reach it!
                            if neighbor_distance < node_table.loc[neighbor, 'distance']:
                                #logging.info("Replacing node connection {}->{} - Distance from origin {}->{}".format(
                                #    visited_anchor, neighbor, node_table.loc[neighbor, 'distance'], neighbor_distance))
                                node_table.loc[neighbor, 'distance'] = neighbor_distance
                                node_table.loc[neighbor, 'parent'] = visited_anchor

                                if self.walking == 'True':
                                    # Can I reach something earlier if I then go by foot?
                                    walking_dist_from_nbor = np.round(
                                        np.abs(self.positions - self.positions[neighbor, :]).dot(
                                            np.array([100 * self.lat_walk_time_for_cent,
                                                      100 * self.lon_walk_time_for_cent])))
                                    wd_from_orig = neighbor_distance + walking_dist_from_nbor

                                    node_table['distance'] = np.concatenate(
                                        (wd_from_orig.reshape(-1, 1), node_table.distance.to_numpy().reshape(-1, 1)),
                                        axis=1).min(axis=1)
                                    node_table.loc[node_table.distance == wd_from_orig, 'parent'] = neighbor

                                    max_dist_from_origin = int(node_table.distance.max())
                                    max_time = add_minutes(starting_time, max_dist_from_origin)

        return idx_a, node_table.distance.tolist()

    def dump_data(self, parameters, database):

        logging.info('Dump generation data to db')

        # Output results
        anchors = pd.DataFrame(self.positions, index=[i for i in range(self.size)], columns=['lat', 'lon'])
        anchors['fk_city_name'] = self.fk_city_name
        anchors['fk_ts_creation'] = self.ts_creation
        anchors = json.loads(anchors.to_json(orient='table'))['data']

        col = database[parameters['coll_anchors']]
        col.insert_many(anchors)

        grid = pd.DataFrame(self.grid, index=[i for i in range(self.size)], columns=[i for i in range(self.size)])
        grid['fk_city_name'] = self.fk_city_name
        grid['fk_ts_creation'] = self.ts_creation
        grid = json.loads(grid.to_json(orient='table'))['data']

        col = database[parameters['coll_grids']]
        col.insert_many(grid)

    def load_data(self, parameters, database):

        col1 = database[parameters['coll_anchors']]
        col2 = database[parameters['coll_grids']]

        anchors = [obj for obj in
                   col1.find({"fk_city_name": self.fk_city_name, "fk_ts_creation": self.ts_creation})]
        t_grid = [obj for obj in
                  col2.find({"fk_city_name": self.fk_city_name, "fk_ts_creation": self.ts_creation})]

        self.positions = pd.DataFrame.from_records(anchors).set_index('index').drop(
            ['_id', 'fk_city_name', 'fk_ts_creation'], axis=1).values
        t_grid = pd.DataFrame.from_records(t_grid).set_index('index').drop(
            ['_id', 'fk_city_name', 'fk_ts_creation'], axis=1)

        t_grid = t_grid.fillna(np.inf)
        self.grid = t_grid.values.tolist()


def add_minutes(tm, minutes):
    full_date = datetime(100, 1, 1, tm.hour, tm.minute, tm.second)
    full_date = full_date + timedelta(minutes=int(minutes))
    return full_date.time()


def query_generations(parameters, database):

    col = database[parameters['coll_generations']]
    query_results_list = [
        obj.get('ts_creation') for obj in col.find(
            {"fk_city_name": parameters['city']}).sort("ts_creation", -1)]

    return {
        "menu": [{
            "idx": idx,
            "ts_creation": obj
        } for idx, obj in enumerate(query_results_list)]
    }
