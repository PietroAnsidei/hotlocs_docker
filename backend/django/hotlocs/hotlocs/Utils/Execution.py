import matplotlib
matplotlib.use('Agg')
# https://stackoverflow.com/questions/44030430/pyplot-crashing-on-plt-figure-in-macos

import gmplot
import hashlib
import json
import logging
import numpy as np
import matplotlib.pyplot as plt

from datetime import datetime

logging.basicConfig(level=logging.INFO, format='%(asctime)s [%(levelname)s] %(message)s', datefmt='%H:%M:%S')


class Execution:

    def __init__(self, parameters, vargs=None, request=None):

        if vargs is None:
            vargs = {'city': None, 'generation': None}
        if request is None:
            destinations = None
        else:
            destinations = request['destinations']

        self.ts_creation = datetime.strftime(datetime.now(), parameters['datetime_fmt'])
        self.destinations = destinations

        self._id = None
        self.fk_gen_id = None
        self.html_path = None

        self.destinations_grid = dict()
        self.destinations_approx = dict()
        self.locations_score = None

        self.city = vargs['city']
        self.generation = vargs['generation']

        # Paths
        self.img_hash = hashlib.md5(self.ts_creation.encode('utf-8')).hexdigest()
        self.heatmap_name = '{}.{}'.format(self.img_hash, parameters['img_ext'])

        self.heat_map = '{}{}'.format(parameters['output_folder'], self.heatmap_name)
        self.html_path = '{}{}.{}'.format(parameters['output_folder'], self.img_hash, parameters['web_ext'])

    def set_destinations(self, parameters):
        with open('{}destinations.json'.format(parameters['input_folder']), 'r') as file_handler:
            load_settings = file_handler.read().replace('\n', '')
        self.destinations = json.loads(load_settings)['destinations']

    def process(self, parameters, database, gmaps, generation):
        # Transform the interest points as their closest approx in the grid
        self.fetch_destinations(gmaps, generation)
        # Compute location score according to the interesting points
        self.compute_locations_score(generation)
        # Plot the heatmap
        self.plot_map(parameters, generation)
        # Save execution json
        self.dump(parameters, database, generation)

    def fetch_destinations(self, gmaps, generation):
        """ Transform the interest points as their closest approx in the grid """

        logging.info('Destination setup')

        for idx, elem in enumerate(self.destinations):

            # in case we don't have the coordinates but just an address, ask google for the coordinates
            if elem['lat'] == 'None' or elem['lon'] == 'None':
                geocode_result = gmaps.geocode(elem['address'])
                elem['lat'] = geocode_result[0]['geometry']['location']['lat']
                elem['lon'] = geocode_result[0]['geometry']['location']['lng']

            coord = np.array((elem['lat'], elem['lon'])).reshape(1, -1)
            closest_idx = np.argmin(np.linalg.norm(generation.positions - coord, axis=1))
            closest = generation.positions[closest_idx, :]

            self.destinations_grid[idx] = closest_idx
            self.destinations_approx[idx] = tuple(closest)

    def compute_locations_score(self, generation):
        """ Compute location score according to the interesting points """

        logging.info('Computing scores')

        self.locations_score = np.zeros((generation.grid_size_lat, generation.grid_size_lon))
        for dest, idx in self.destinations_grid.items():

            # Adding distance to reach the interest point
            self.locations_score += np.array(
                generation.grid[idx]).reshape(
                (generation.grid_size_lat, generation.grid_size_lon), order='F'
            )

        self.locations_score = 1 / (self.locations_score + 1)  # to avoid division by zero

    def plot_heatmap(self, img_bounds, parameters):
        """ Plot the heatmap """

        logging.info('Plotting heatmap')

        fig = plt.figure(frameon=True)
        ax = plt.Axes(fig, [0., 0., 1., 1.])
        ax.set_aspect('equal')
        ax.set_axis_off()
        ax.tick_params(which='both', direction='in')
        fig.add_axes(ax)  # Plot the data

        ax.imshow(self.locations_score, cmap='coolwarm', alpha=0.6, extent=[
            img_bounds['west'], img_bounds['east'], img_bounds['north'], img_bounds['south']
        ])
        # Save figure to ensure image has no padding around it.
        fig.savefig(self.heat_map, format=parameters['img_ext'], dpi=1000, transparent=True, bbox_inches='tight',
                    pad_inches=0)

    def plot_map(self, parameters, generation):
        """ Utility to plot the heatmap on a gmap """

        lat_midpt = np.mean([generation.map_b['south'], generation.map_b['north']])
        lon_midpt = np.mean([generation.map_b['west'], generation.map_b['east']])
        resize_factor_lat = generation.grid_size_lat / (generation.grid_size_lat - 1)
        resize_factor_lon = generation.grid_size_lon / (generation.grid_size_lon - 1)

        img_bounds = {
            'north': (generation.map_b['north'] - lat_midpt) * resize_factor_lat + lat_midpt,
            'south': (generation.map_b['south'] - lat_midpt) * resize_factor_lat + lat_midpt,
            'west': (generation.map_b['west'] - lon_midpt) * resize_factor_lon + lon_midpt,
            'east': (generation.map_b['east'] - lon_midpt) * resize_factor_lon + lon_midpt
        }
        self.plot_heatmap(img_bounds, parameters)

        logging.info('Plotting gmap')

        gmap = gmplot.GoogleMapPlotter(lat_midpt, lon_midpt, zoom=15.5, apikey=parameters['g_key'])

        # Overlay the heatmap layer
        # REQ pip3 install git+git://github.com/vgm64/gmplot@d53cd1cf4767f4d147834eb915b0daccdd0bcd27#egg=gmplot
        gmap.ground_overlay(self.heatmap_name, img_bounds)  # Plot the raw data

        lats_true = [loc['lat'] for loc in self.destinations]
        lons_true = [loc['lon'] for loc in self.destinations]

        lats_appr = [loc[0] for dest, loc in self.destinations_approx.items()]
        lons_appr = [loc[1] for dest, loc in self.destinations_approx.items()]

        gmap.scatter(lats_appr, lons_appr, color='black', size=15, marker=False)  # Draw approx dest and save locally
        gmap.scatter(lats_true, lons_true, color='red', size=15, marker=True)  # Draw dest and save locally
        gmap.draw(self.html_path)

    def dump(self, parameters, database, generation):
        logging.info("Saving execution")

        col = database[parameters['coll_executions']]
        col.insert_one(
            {
                'destinations': self.destinations,
                'html_path': self.html_path.split("/")[-1],
                'timestamp': self.ts_creation,
                'fk_city_name': self.city,
                'fk_ts_creation': generation.ts_creation
            }
        )
