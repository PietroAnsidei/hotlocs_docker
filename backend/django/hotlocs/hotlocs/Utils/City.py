import json
import logging
import os
import pandas as pd

from datetime import datetime

logging.basicConfig(level=logging.INFO, format='%(asctime)s [%(levelname)s] %(message)s', datefmt='%H:%M:%S')


class City:

    def __init__(self, _id=None, city_name=None, version=None, map_b=None, stops=None, stop_times=None):
        self._id = _id
        self.city_name = city_name
        self.version = version
        self.map_b = map_b
        self.stops = stops
        self.stop_times = stop_times

    @classmethod
    def from_dict(cls, json_dict):
        return cls(**json_dict)

    def to_json(self):
        return dict(city_name=self.city_name, version=self.version, map_b=self.map_b)

    @staticmethod
    def load_or_create(parameters, database, execution):

        col1 = database[parameters['coll_cities']]
        col2 = database[parameters['coll_stops']]
        col3 = database[parameters['coll_stoptimes']]

        try:
            logging.info('Importing city data from db')
            city = City.from_dict(col1.find({"city_name": execution.city}).sort("version", -1).limit(1)[0])

            if city.version is None:
                logging.info('City not found! Creating city')
                city = City(city_name=execution.city, version=execution.ts_creation)
                city._id = col1.insert_one(city.to_json()).inserted_id

            if parameters['reload']:
                logging.info('Importing stops from db')
                city.stops = [obj for obj in col2.find({"city": city.city_name, "version": city.version})]
                logging.info('Importing stoptimes from db')
                city.stop_times = [obj for obj in col3.find({"city": city.city_name, "version": city.version})]
                if not city.stops or not city.stop_times:
                    raise ValueError('Stops or Stoptimes are not defined!')

        except ValueError as e:
            logging.warning(e)

            data = city.data_import(parameters)

            logging.info('Exporting stops data to db')

            city.stops = data['stops'].to_dict('records')
            col2.insert_many(city.stops)

            logging.info('Exporting stoptimes data to db')

            city.stop_times = data['stop_times'].to_dict('records')
            col3.insert_many(city.stop_times)

        logging.info('Operation complete!')

        return city

    def data_import(self, parameters):
        data = dict()
        for source in parameters['sources'].keys():
            for folder in os.listdir(parameters['input_folder']):

                file_name = '{}/{}.{}'.format(folder, source, parameters['dat_ext'])
                complete_path = '{}{}'.format(parameters['input_folder'], file_name)

                if os.path.exists(complete_path):
                    logging.info('Importing {}'.format(complete_path))

                    imported = pd.read_csv(complete_path, usecols=parameters['sources'][source])
                    imported = imported.dropna(axis=1, how='all')
                    imported['city'] = self.city_name
                    imported['version'] = self.version

                    if source not in data:
                        data[source] = imported
                    else:
                        data[source] = pd.concat([data[source], imported])

            if source == 'stops':
                # Filtering stops data within the city limits
                bounds_lat = [float(self.map_b['north']), float(self.map_b['south'])]
                bounds_lon = [float(self.map_b['east']), float(self.map_b['west'])]

                data[source] = data[source][
                    data[source].stop_lat.between(min(bounds_lat), max(bounds_lat)) &
                    data[source].stop_lon.between(min(bounds_lon), max(bounds_lon))
                ]

            if source == 'stop_times':
                if 'stops' in data:
                    # Filtering stop_times data within the pre-filtered stops
                    times = data[source][
                        data[source].stop_id.isin(data['stops'].stop_id.unique())
                    ].copy()
                else:
                    times = data[source].copy()

                times.arrival_time = times.arrival_time.apply(
                    lambda x: '0' + x if x[1] == ':' else x)
                times.departure_time = times.departure_time.apply(
                    lambda x: '0' + x if x[1] == ':' else x)
                times.arrival_time = times.arrival_time.apply(
                    lambda x: '{:02d}{}'.format(int(x[:2]) - 24, x[2:]) if int(x[:2]) > 23 else x)
                times.departure_time = times.departure_time.apply(
                    lambda x: '{:02d}{}'.format(int(x[:2]) - 24, x[2:]) if int(x[:2]) > 23 else x)

                data[source] = times

            data[source] = data[source].drop_duplicates()

        return data


def query_cities(parameters, database):

    col = database[parameters['coll_cities']]
    query_results_list = [obj.get('city_name') for obj in col.find({})]

    return {
        "menu": [{
            "idx": idx,
            "city": obj
        } for idx, obj in enumerate(list(set(query_results_list)))]
    }


def post_cities(parameters, database, request):

    col = database[parameters['coll_cities']]

    city = City.from_dict(request.data)
    city.version = datetime.strftime(datetime.now(), parameters['datetime_fmt'])
    city._id = col.insert_one(city.to_json()).inserted_id

    return city.to_json()
