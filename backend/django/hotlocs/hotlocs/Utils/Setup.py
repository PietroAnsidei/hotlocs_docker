import argparse
import configparser
import os

from pymongo.mongo_client import MongoClient


def parse_input():
    parser = argparse.ArgumentParser()

    parser.add_argument('--multi', action='store_true', dest='multi_thread', default=False,
                        help='enable multithreading')
    parser.add_argument('--city', type=str, dest='city', default='',
                        help='city to compute')
    parser.add_argument('--reload', action='store_true', dest='reload', default=False,
                        help='compute the map back again')
    parser.add_argument('--generation', type=str, dest='generation', default='latest',
                        help='city to compute')

    return vars(parser.parse_args())


def global_params(vargs):
    params = dict()

    params['multi_thread'] = vargs['multi_thread']
    params['reload'] = vargs['reload']
    if 'city' in vargs:
        params['city'] = vargs['city']

    # Data extension
    params['dat_ext'] = 'txt'
    params['img_ext'] = 'png'
    params['web_ext'] = 'html'
    params['dict_ext'] = 'json'

    # Time setup parameters
    params['time_fmt'] = '%H:%M:%S'
    params['datetime_fmt'] = '%Y-%m-%d_%H%M%S'

    # Google api key
    config = configparser.ConfigParser()
    config.read("config.ini")
    params['g_key'] = config.get("google_key", "google_maps")

    # Db params
    params['db_name'] = 'hotlocs'
    params['host'] = '127.0.0.1'
    params['port'] = 27017
    params['coll_cities'] = 'cities'
    params['coll_stops'] = 'stops'
    params['coll_stoptimes'] = 'stoptimes'
    params['coll_generations'] = 'generations'
    params['coll_anchors'] = 'anchors'
    params['coll_grids'] = 'grids'
    params['coll_executions'] = 'executions'

    # Data to import
    params['data_folders'] = ['gtfs', 'osm']
    params['sources'] = {
        'stops': ['stop_id', 'stop_name', 'stop_lat', 'stop_lon'],
        'stop_times': ['trip_id', 'arrival_time', 'departure_time', 'stop_id', 'stop_sequence']
    }

    # Paths
    params['base_folder'] = './Data/'
    params['input_folder'] = '{}{}'.format(params['base_folder'], 'input/')
    params['output_folder'] = '{}{}'.format(params['base_folder'], 'output/')
    os.makedirs(params['output_folder'], exist_ok=True)

    return params


def db_config(db_params):
    client = MongoClient(host=db_params['host'], port=db_params['port'])
    # username="mongoAdmin",
    # password="changeMe",
    # authSource="admin"

    return client[db_params['db_name']]
