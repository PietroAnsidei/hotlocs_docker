# hotlocs

HotLocs v1.0 multicity e multiservizio. Ora è possibile testare su qualsiasi città che abbia il sistema GTFS con precisione a piacere. La procedura è la seguente:

0) Creare nella home di progetto un file config.ini contenente la google api key nel modo seguente

    [google_key]
    google_maps: your_api_key

1) Creare una cartella Data nella home di progetto (se non presente)
2) Entrare in Data e creare una cartella <nome_città>
3) Entrare nella cartella <nome_città>
4) Scaricare e unzippare in <nome_città> i file zip gtfs di transitfeed per la città desiderata
5) Dopo estratto lo zip, verificare che in <nome_città> sia essere presente la sottocartella <ditta_trasporti>_gtfs
6) Verificare che all'interno di <ditta_trasporti>_gtfs siano presenti i file stops.txt e stop_times.txt
7) Ripetere le operazioni (4-5-6) per tutti i servizi desiderati nella città (es. ATM, Trenord per Milano)
8) Inserire all'interno della home i file <destinations.json> e (in caso di reload) <generation.json>
9) Il file <generation.json> contiene un dizionario con il nome della città, i limiti (nord, sud, ovest, est) della mappa, il numero di quadratini desiderati in latitudine e longitudine, il parametro walking. Setuppare a piacere il file in caso di reload.
10) Il file <destinations.json> contiene un dizionario con i punti di interesse.
11) Ritornare alla home di progetto e lanciare il codice seguente

	python3 main.py --city <nome_città> [--reload] | [--generation <latest> | <generation_id> ]

11) Nel caso si intenda operare un ricalcolo della città (es. per aggiungere mezzi di trasporto alternativi, nuove funzionalità o attivare/disattivare walking) è possibile lanciare il codice con l'opzione --reload
12) Nel caso si vogliano cambiare i punti di interesse della città è sufficiente modificare <destinations.json> e lanciare il codice come in 10)
