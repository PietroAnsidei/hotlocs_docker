import googlemaps

from Utils.Setup import *
from Utils.City import *
from Utils.Generation import *
from Utils.Execution import *

logging.basicConfig(level=logging.INFO, format='%(asctime)s [%(levelname)s] %(message)s', datefmt='%H:%M:%S')


if __name__ == '__main__':

    # Setup main params
    u_args = parse_input()
    parameters = global_params(u_args)

    # Setup db connection & google api key
    database = db_config(parameters)
    gmaps = googlemaps.Client(key=parameters['g_key'])

    # Create objects
    execution = Execution(parameters, u_args)
    city = City.load_or_create(parameters, database, execution)
    generation = Generation.load_or_create(parameters, database, city, execution)

    # Process data and output
    execution.set_destinations(parameters)
    generation.process(parameters, database, city)
    execution.process(parameters, database, gmaps, generation)
