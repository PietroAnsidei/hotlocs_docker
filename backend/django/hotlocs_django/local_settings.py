DATABASES = {
    'default': {
        'ENGINE': 'djongo',
        'NAME': 'hotlocs',
        'CLIENT': {
            'host': '127.0.0.1',
            'port': 27017,
            'username': 'admin',
            'password': 'myadminpassword',
        }
    }
}

#################################################################
    ##  (CORS) Cross-Origin Resource Sharing Settings ##
#################################################################
CORS_ORIGIN_ALLOW_ALL = True