#!/bin/bash
sudo ulimit -n 64000
sudo ulimit -u 64000
sudo cat << EOF > /etc/yum.repos.d/mongodb-org-4.4.repo
[mongodb-org-4.4]
name=MongoDB Repository
baseurl=https://repo.mongodb.org/yum/redhat/7Server/mongodb-org/4.4/x86_64/
gpgcheck=1
enabled=1
gpgkey=https://www.mongodb.org/static/pgp/server-4.4.asc
EOF
sudo yum -y install mongodb-org
sudo systemctl start mongod
sudo systemctl enable mongod

cat << EOF > create.js
use admin
db.createUser({ user: "admin", pwd: "myadminpassword", roles: [ { role: "userAdminAnyDatabase", db: "admin" }, { role: "readWriteAnyDatabase", db: "admin" }, { role: "dbAdminAnyDatabase",   db: "admin" } ]  })
EOF

cat << EOF > createdb.js
use hotlocs
EOF

mongo admin < create.js
mongo admin < createdb.js
sudo sed -i 's/127.0.0.1/0.0.0.0/' /etc/mongod.conf

sudo systemctl restart mongod

